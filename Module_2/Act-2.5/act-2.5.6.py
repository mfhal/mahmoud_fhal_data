def Pal_(ch):
       x=len(ch)-1
       result=1
       i=0
       while (i<=x) and (result==1):
              if ch[i] != ch[x]:
                     result=0
              i=i+1
              x=x-1
       return result

s=input('entrer un mot : ')
if Pal_(s):
       print(s,'est un palindrome')
else: print(s,'n''\'est pas palindrome')