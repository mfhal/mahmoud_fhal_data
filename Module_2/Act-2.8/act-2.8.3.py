class Livres:
    genres = ("Bande dessinné", "Mangas", "Roman", "livre recette")
    
    def __init__(self, titre, auteur, prix, nbre_pages, genre=''):
        self.titre = titre
        self.auteur = auteur
        self.prix = prix
        self.nbre_pages = nbre_pages
        self.genre = genre

    def __str__(self):
        return f"le livre {self.titre}, l'oeuvre de {self.auteur} comporte {self.nbre_pages} pages " \
               f"et coute {self.prix} €. "
        
    
class BandesDess (Livres):
    lecture = "de gauche à droite"

    def __init__(self, titre, auteur, prix, nbre_pages, couleur):
        super().__init__(titre, auteur, prix, nbre_pages)
        self.lecture = BandesDess.lecture
        if couleur:
            self.couleur = "couleur"
        else: 
            self.couleur = "noir et blanc"
    
    def __str__(self):
        return (f"la BD {self.titre}, l'oeuvre de {self.auteur} comporte {self.nbre_pages} pages , "
                f"coute {self.prix} €. Elle en {self.couleur} et se lit {self.lecture}.")
        
        
class Mangas (Livres):
    couleur = "noir et blanc"
    lecture = "de droite vers la gauche"

    def __init__(self, titre, auteur, prix, nbre_pages):
        super().__init__(titre, auteur, prix, nbre_pages)
        self.lecture = Mangas.lecture
        self.couleur = Mangas.couleur
    
    def __str__(self):
        return (f"le Manga {self.titre}, l'oeuvre de {self.auteur} comporte {self.nbre_pages} pages , "
                f"coute {self.prix} €. Elle en {self.couleur} et se lit {self.lecture}.")


class Roman (Livres):
    def __init__(self, titre, auteur, nbre_pages, prix, nbre_chapitre=0, resume=''):
        super().__init__(titre, auteur, prix, nbre_pages)
        self.nbre_chapitre = nbre_chapitre
        self.resume = resume
    
    def __str__(self):
        return (f"le roman {self.titre}, l'oeuvre de {self.auteur} comporte {self.nbre_pages} pages, "
                f"coute {self.prix}€. Il contient {self.nbre_chapitre} chapitres.")


class Livre_recettes (Livres):
    def __init__(self, titre, auteur, prix, nbre_recette=0, nbre_pages=0):
        super().__init__(titre, auteur, prix, nbre_pages)
        self.nbre_recette = nbre_recette
    
    def __str__(self):
        return (f"le livre des recettes {self.titre}, l'oeuvre de {self.auteur} "
                f"comporte {self.nbre_recette} recettes et coute {self.prix}€.")
    
    def ajout_recette(self, recette):
        self.nbre_recette += 1
    

class Recette:
    etapes = []

    def __init__(self, nom, desc, niv_diff):
        self.nom = nom
        self.desc = desc
        self.niv_diff = niv_diff
        self.etapes = Recette.etapes
    
    def __str__(self):
        return (f"la recette {self.nom}, detaille  {self.desc}. Elle est de niveau difficulté {self.niv_diff} "
                f"et comporte {len(self.etapes)} etapes.")
    
    def ajout_etape(self, etape):
        Recette.etapes.append(etape)


l1 = Livres("Le petit prince", "StExupéry", 10.40, 50)
l2 = Livres("Contes", "Grimm", 14.40, 254)

print(l1)
print(l2)

b1 = BandesDess("Lucky Luke", "Morris", 10.40, 45, True)
b2 = BandesDess("Tintin", "Herge", 200.40, 45, False)
print(b1)
print(b2)

m1 = Mangas("One piece", "Eiichirō Oda", 5.40, 62)
m2 = Mangas("Death Note", "Tsugumi Ōba", 7.40, 75)
print(m1)
print(m2)

r1 = Roman("Dora", "Dora", 300, 3.5)
print(r1)
r1.nbre_chapitre = 12
r1.resume = "Une description quelconque"
print(r1)

lrc1 = Livre_recettes("Marmiton", "Philippe Etchebest", 15.98, 110)
print(lrc1)

rc1 = Recette("Les pâtes crues", "Comment réaliser de délicieuses pâtes crues.", 3)
print(rc1)       

rc1.ajout_etape("Ne pas les faire cuire.")    
print(rc1)
rc1.ajout_etape("Sortir les pâtes de leur emballage")    
print(rc1)

lrc1.ajout_recette(rc1)
print(lrc1)
