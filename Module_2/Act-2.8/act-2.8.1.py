class Compte:
    code=0
    def __init__(self,solde=0):
        Compte.code += 1
        self.code=Compte.code
        self.solde = solde
        
    def deposer(self,argent):
        argent=int(input("le montant à déposer: "))
        self.solde += argent
        
    def retirer(self, argent):
        argent=int(input("le montant à retirer: "))
        self.solde -= argent  
        
    def __str__(self):
        return(f'le solde du compte {self.code} est : {self.solde:,.2f}€'.replace(",", " ").replace(".", ","))

class CompteEpargne (Compte):
    def calculInteret(self,interet=6):
        if self.solde>0:
            self.solde +=(self.solde/100)*interet
        
        print(f'calculInteret={self.solde :,.2f} €')   

class ComptePayant (Compte):
    def retirer(self,argent):
        super().retirer(argent)
        self.solde-=2
     
        




compte=Compte(1000)
print(compte)
compte.deposer(2000)
print(compte)
compte.retirer(200)
print(compte)

compte_1=CompteEpargne()
print(compte_1)
compte_1.deposer(1000)
print(compte_1)
compte_1.retirer(100)
print(compte_1)
compte_1.calculInteret()



compte_2=ComptePayant()
print(compte_2)
compte_2.deposer(1500)
print(compte_2)
compte_2.retirer(100)
print(compte_2)
compte_2.deposer(500)
print(compte_2)


compte_3=Compte(100)
print(compte_3)
compte_3.deposer(200)
print(compte_3)
compte_3.retirer(20)
print(compte_3)
    
print(compte_1.code)
print(compte_2.code)
print(compte_3.code)

print("nbre de compte",Compte.code)
