class Produit:
    code = 0

    def __init__(self, nom, prix=0, description=''):
        Produit.code += 1
        self.code = Produit.code
        self.nom = nom
        self.prix = prix
        self.description = description
    
    def afficher_prix(self):
        print(f"le prix de {self.nom} est {self.prix:,.2f}€".replace(",", " ").replace(".", ","))
        
    def afficher_desc(self):
        print(f"le {self.nom} est un/e {self.description}")


class Poubelle (Produit):
    def __init__(self, nom, prix, description):
        super().__init__(nom, prix, description)
    
    def afficher_prix(self):
        super().afficher_prix()
        
    def afficher_desc(self):
        super().afficher_desc()


class Balai (Produit):
    def __init__(self, nom, prix, description):
        super().__init__(nom, prix, description)
    
    def afficher_prix(self):
        super().afficher_prix()
        
    def afficher_desc(self):
        super().afficher_desc()


poubelle = Poubelle('poubelle', 100, 'premier choix')
print(poubelle.code)
balai = Balai('Balais', 2500, 'excellente affaire')
balai.afficher_prix()
balai.afficher_desc()
print(balai.code)
