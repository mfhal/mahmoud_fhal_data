### Select With a Filter

import mysql.connector
mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  password="AzertyUI#2022!",
  database="my_new_database"
) 
mycursor = mydb.cursor(buffered=True)


sql = "SELECT * FROM customers WHERE address ='Park Lane 38'"
mycursor.execute(sql)
myresult = mycursor.fetchall()
for x in myresult:
  print(x)


sql = "SELECT * FROM customers WHERE address LIKE '%way%'"  
mycursor.execute(sql)
myresult = mycursor.fetchall()
for x in myresult:
    print('\n',x) 

# Order_By
sql = "SELECT * FROM customers ORDER BY name"    
mycursor.execute(sql)    
myresult = mycursor.fetchall()
for x in myresult:
      print(x)

sql = "SELECT * FROM customers ORDER BY name DESC"      
mycursor.execute(sql)      
myresult = mycursor.fetchall()
for x in myresult:
        print(x) 

# Delete_Record
sql = "DELETE FROM customers WHERE address = 'Mountain 21'"
mycursor.execute(sql)
mydb.commit()        
print(mycursor.rowcount, "record(s) deleted")

# Update_Table:
sql = "UPDATE customers SET address = 'Canyon 123' WHERE address = 'Valley 345'"
mycursor.execute(sql)
mydb.commit()
print(mycursor.rowcount, "record(s) affected")

#Limit_Result
mycursor.execute("SELECT * FROM customers LIMIT 5")
myresult = mycursor.fetchall()
for x in myresult:
  print(x)

mycursor.execute("SELECT * FROM customers LIMIT 5 OFFSET 2")  
myresult = mycursor.fetchall()  
for x in myresult:
  print('blabla',x) 