import mysql.connector
mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  password="AzertyUI#2022!",
  database="my_new_database"
) 
mycursor = mydb.cursor(buffered=True)

mycursor.execute("SET FOREIGN_KEY_CHECKS=0")
mycursor.execute("DROP TABLE IF EXISTS EMP")
mycursor.execute("DROP TABLE IF EXISTS DEPT")

mycursor.execute("CREATE TABLE EMP (ENO VARCHAR(255) PRIMARY KEY, ENOM VARCHAR(255), PROF VARCHAR(255), SAL INTEGER(255), COMM INTEGER(255), DNO VARCHAR(255))")

mycursor.execute("CREATE TABLE DEPT (DNO VARCHAR(255) PRIMARY KEY, DNOM VARCHAR(255), DIR VARCHAR(255), VILLE VARCHAR(255))")



sql_1 = "INSERT INTO EMP (ENO, ENOM, PROF, SAL, COMM, DNO) VALUES (%s, %s, %s, %s, %s, %s)"

sql_2 = "INSERT INTO DEPT (DNO, DNOM, DIR, VILLE) VALUES (%s, %s, %s, %s)"

val_1 =  [
  ('10', 'Mahmoud','Pharmacien','5000', '5000', '3'),
  ('20', 'Bechir', 'Technicien', '3000', '2000', '2'),
  ('30', 'Souha','Vendeur','6000', '3000', '1'),
  ('40', 'Karim', 'Technicien', '3000', '2000', '3'),
  ('50', 'Fayek', 'Ingenieur', '5000', '2000', '2'),
]

val_2 =  [
  ('1', 'Commercial','30', 'Paris'),
  ('2', 'Production','50', 'Tunis'),
  ('3', 'Assurance_qualité','10', 'Tunis'),
]

mycursor.executemany(sql_1, val_1)
mycursor.executemany(sql_2, val_2)
mydb.commit()
print(mycursor.rowcount, "was inserted.")

mycursor.execute("ALTER TABLE EMP ADD foreign key(DNO) references DEPT(DNO)")

mycursor.execute("ALTER TABLE DEPT ADD foreign key(DIR) references EMP(ENO)")

#QST1:
sql1 = "SELECT * FROM EMP WHERE SAL+COMM >= 10000"
mycursor.execute(sql1)
myresult1 = mycursor.fetchall()
for x in myresult1:
  print('les employés les plus riches \n',x)

#QST2:
sql2 = "SELECT \
        EMP.ENOM,\
        EMP.PROF \
        FROM EMP\
        WHERE ENO = 10"
mycursor.execute(sql2)
myresult2 = mycursor.fetchall()
for x in myresult2:
  print('Employé du mois \n',x)
  
#QST3:
sql3 = "SELECT \
    EMP.ENOM AS Name, \
    DEPT.VILLE AS Ville \
    FROM EMP \
    INNER JOIN DEPT ON EMP.DNO = DEPT.DNO \
    WHERE DEPT.VILLE = 'Paris'"
mycursor.execute(sql3)
myresult3 = mycursor.fetchall()  
for x in myresult3:
    print('the french employee \n',x) 

#QST4:    
sql4 = "SELECT \
    EMP.ENOM AS Name \
    FROM EMP \
    INNER JOIN DEPT ON EMP.ENO = DEPT.DIR \
    WHERE DNOM = 'Commercial'"
mycursor.execute(sql4)
myresult4 = mycursor.fetchall()  
for x in myresult4:
    print('Notre directeur commercial est: \n',x)

#QST5:
sql5 = "SELECT \
    EMP.PROF AS Profession, \
    DEPT.DNOM AS Departement \
    FROM EMP \
    INNER JOIN DEPT ON EMP.ENO = DEPT.DIR "
mycursor.execute(sql5)
myresult5 = mycursor.fetchall()  
for x in myresult5:
    print('Nos directeurs sont: \n',x)

#QST:6
sql6 = "SELECT \
        EMP.ENOM, \
        DEPT.DNOM \
        FROM EMP \
        INNER JOIN DEPT ON EMP.ENO = DEPT.DIR \
        WHERE PROF = 'Ingenieur'"
mycursor.execute(sql6)
myresult6 = mycursor.fetchall()  
for x in myresult6:
    print('Nos ing sont \n',x)
    
