import mysql.connector
mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  password="AzertyUI#2022!",
  database="my_new_database"
) 
mycursor = mydb.cursor(buffered=True)

mycursor.execute("SET FOREIGN_KEY_CHECKS=0")
mycursor.execute("DROP TABLE IF EXISTS ACTEURS")
mycursor.execute("DROP TABLE IF EXISTS FILMS")

mycursor.execute("CREATE TABLE ACTEURS (titre CHAR(50), Acteur CHAR(20))")
mycursor.execute("CREATE TABLE FILMS (titre CHAR(50), pays CHAR(10), année INTEGER(4), réalisateur CHAR(20), durée INTEGER(3))")
sql_1 = "INSERT INTO FILMS (titre, pays, année, réalisateur, durée) VALUES (%s, %s, %s, %s, %s)"
sql_2 = "INSERT INTO ACTEURS (titre, Acteur) VALUES (%s, %s)"

val_1 =  [
        ('le magicien', 'France',2001, 'Francois truffant', 135),
      ('the lord', 'France',1985 , 'sébastien', 130),
      ('breaking bad', 'USA',2010, 'Mahmoud', 120),
]

val_2 =  [
  ('le magicien', 'Francois truffant'),
      ('the lord','Samir'),
      ('the lord', 'catherine deneuve'),
      ('BREAKING BAD', 'SAUL'),
      ('le magicien', 'sébastien'),
      ('le magicien', 'Jean Gabin'),
      ('le magicien', ' Damien'),
]

mycursor.executemany(sql_1, val_1)
mycursor.executemany(sql_2, val_2)
mydb.commit()
print(mycursor.rowcount, "was inserted.")

# les films Francais


sql = "SELECT titre, année ,réalisateur FROM FILMS WHERE   pays = 'France' "
mycursor.execute(sql)
myresult = mycursor.fetchall()
for x in myresult:
    print(x)

# les années de sorties des films dont Jean Gabin est un acteur


mycursor.execute("SELECT année FROM FILMS INNER JOIN ACTEURS ON ACTEURS.titre = FILMS.titre WHERE  ACTEURS.acteur='Jean Gabin'")
myresult = mycursor.fetchall()
for x in myresult:
    print(x)

# Francois Truffaut

mycursor.execute("SELECT acteur FROM ACTEURS INNER JOIN FILMS ON FILMS.titre = ACTEURS.titre WHERE FILMS.réalisateur='Francois Truffaut'")
myresult = mycursor.fetchall()
for x in myresult:
    print(x)

#Catherine Deneuve

mycursor.execute("SELECT FILMS.titre FROM FILMS JOIN ACTEURS ON FILMS.titre = ACTEURS.titre WHERE ACTEURS.acteur='Catherine Deneuve'")
myresult = mycursor.fetchall()
for x in myresult:
    print(x)

mycursor.execute("SELECT ACTEURS.acteur FROM ACTEURS  WHERE ACTEURS.titre ='the lord' ")
myresult = mycursor.fetchall()
for x in myresult:
    print(x)

# Réalisateur aussi acteur

mycursor.execute("SELECT ACTEURS.titre FROM ACTEURS JOIN FILMS ON FILMS.titre = ACTEURS.titre WHERE ACTEURS.acteur=FILMS.réalisateur")
myresult = mycursor.fetchall()
for x in myresult:
    print(x)
#

mycursor.execute("SELECT * FROM FILMS JOIN ACTEURS ON FILMS.réalisateur = ACTEURS.acteur WHERE ACTEURS.titre <> FILMS.titre ")
myresult = mycursor.fetchall()
for x in myresult:
    print(x)
#
mycursor.execute("SELECT réalisateur FROM FILMS JOIN ACTEURS ON FILMS.réalisateur = ACTEURS.acteur WHERE ACTEURS.titre <> FILMS.titre ")
myresult = mycursor.fetchall()
for x in myresult:
    print(x)

#

mycursor.execute("SELECT acteur FROM ACTEURS JOIN FILMS ON FILMS.titre = ACTEURS.titre WHERE FILMS.réalisateur='Francois Truffaut'")
myresult = mycursor.fetchall()
for x in myresult:
    print(x)