import pandas
### help(pandas)

mydataset = {
  'cars': ["BMW", "Volvo", "Ford"],
  'passings': [3, 7, 2]
}
myvar = pandas.DataFrame(mydataset)
print(myvar) 

### 0    BMW         3
### 1  Volvo         7
### 2   Ford         2

import pandas as pd
mydataset = {
  'cars': ["BMW", "Volvo", "Ford"],
  'passings': [3, 7, 2]
}
myvar = pd.DataFrame(mydataset)
print(myvar)

### 0    BMW         3
### 1  Volvo         7
### 2   Ford         2


print(pd.__version__)
### 1.4.3

a = [1, 7, 2]
myvar = pd.Series(a)
print(myvar)

### 0    1
### 1    7
### 2    2
### dtype: int64

print(myvar[0])
### 1

a = [1, 7, 2]
myvar = pd.Series(a, index = ["x", "y", "z"])
print(myvar)

### x    1
### y    7
### z    2
### dtype: int64

print(myvar["y"])
### 7

calories = {"day1": 420, "day2": 380, "day3": 390}
myvar = pd.Series(calories)
print(myvar)
### day1    420
### day2    380
### day3    390
### dtype: int64

calories = {"day1": 420, "day2": 380, "day3": 390}
myvar = pd.Series(calories, index = ["day1", "day2"])
print(myvar)
### day1    420
### day2    380
### dtype: int64

data = {
  "calories": [420, 380, 390],
  "duration": [50, 40, 45]
}
myvar = pd.DataFrame(data)
print(myvar)
###    calories  duration
### 0       420        50
### 1       380        40
### 2       390        45

data = {
  "calories": [420, 380, 390],
  "duration": [50, 40, 45]
}
df = pd.DataFrame(data)
#refer to the row index:
print(df.loc[0])
### calories    420
### duration     50
### Name: 0, dtype: int64

print(df.loc[[0, 1]])
###    calories  duration
### 0       420        50
### 1       380        40

df = pd.DataFrame(data, index = ["day1", "day2", "day3"])
print(df)
###       calories  duration
### day1       420        50
### day2       380        40
### day3       390        45

print(df.loc["day2"])
### calories    380
### duration     40
### Name: day2, dtype: int64

try:
    df = pd.read_csv('data.csv')
    print(df)
except FileNotFoundError:
    print('data.csv n''\'existe pas')
###      Duration  Pulse  Maxpulse  Calories
### 0          60    110       130     409.1
### 1          60    117       145     479.0
### 2          60    103       135     340.0
### 3          45    109       175     282.4
### 4          45    117       148     406.0
### ..        ...    ...       ...       ...
### 164        60    105       140     290.8
### 165        60    110       145     300.0
### 166        60    115       145     310.2
### 167        75    120       150     320.4
### 168        75    125       150     330.4

### [169 rows x 4 columns]

print(df.to_string())
### use to_string() to print the entire DataFrame.

print(pd.options.display.max_rows) 
### 60
### In my system the number is 60, which means that if the DataFrame contains more than 60 rows, 
### the print(df) statement will return only the headers and the first and last 5 rows.

pd.options.display.max_rows = 9999
df = pd.read_csv('data.csv')
print(df)

try:
    import pandas as pd
    df = pd.read_json('data.json')
    print(df.to_string())
except ValueError:
    print('data.json n''\'existe pas')

data = {
  "Duration":{
    "0":60,
    "1":60,
    "2":60,
    "3":45,
    "4":45,
    "5":60
  },
  "Pulse":{
    "0":110,
    "1":117,
    "2":103,
    "3":109,
    "4":117,
    "5":102
  },
  "Maxpulse":{
    "0":130,
    "1":145,
    "2":135,
    "3":175,
    "4":148,
    "5":127
  },
  "Calories":{
    "0":409,
    "1":479,
    "2":340,
    "3":282,
    "4":406,
    "5":300
  }
}

df = pd.DataFrame(data)

print(df)

import pandas as pd
df = pd.read_csv('data.csv')
print(df.head())
###    Duration  Pulse  Maxpulse  Calories
### 0        60    110       130     409.1
### 1        60    117       145     479.0
### 2        60    103       135     340.0
### 3        45    109       175     282.4
### 4        45    117       148     406.0

print(df.tail())
###      Duration  Pulse  Maxpulse  Calories
### 164        60    105       140     290.8
### 165        60    110       145     300.0
### 166        60    115       145     310.2
### 167        75    120       150     320.4
### 168        75    125       150     330.4

print(df.info()) 
### <class 'pandas.core.frame.DataFrame'>
### RangeIndex: 169 entries, 0 to 168
### Data columns (total 4 columns):
###  #   Column    Non-Null Count  Dtype  
### ---  ------    --------------  -----  
###  0   Duration  169 non-null    int64  
###  1   Pulse     169 non-null    int64  
###  2   Maxpulse  169 non-null    int64  
###  3   Calories  164 non-null    float64
### dtypes: float64(1), int64(3)
### memory usage: 5.4 KB

df = pd.read_csv('data.csv')

new_df = df.dropna()

print(new_df.info())
### <class 'pandas.core.frame.DataFrame'>
### Int64Index: 164 entries, 0 to 168
### Data columns (total 4 columns):
###  #   Column    Non-Null Count  Dtype  
### ---  ------    --------------  -----  
###  0   Duration  164 non-null    int64  
###  1   Pulse     164 non-null    int64  
###  2   Maxpulse  164 non-null    int64  
###  3   Calories  164 non-null    float64
### dtypes: float64(1), int64(3)
### memory usage: 6.4 KB

df = pd.read_csv('data.csv')

df.dropna(inplace = True)

print(df.info())
### Int64Index: 164 entries, 0 to 168
### Data columns (total 4 columns):
###  #   Column    Non-Null Count  Dtype  
### ---  ------    --------------  -----  
###  0   Duration  164 non-null    int64  
###  1   Pulse     164 non-null    int64  
###  2   Maxpulse  164 non-null    int64  
###  3   Calories  164 non-null    float64
### dtypes: float64(1), int64(3)
### memory usage: 6.4 KB

df = pd.read_csv('data.csv')
df.fillna(130, inplace = True) 
print(df.info())
### <class 'pandas.core.frame.DataFrame'>
### RangeIndex: 169 entries, 0 to 168
### Data columns (total 4 columns):
###  #   Column    Non-Null Count  Dtype  
### ---  ------    --------------  -----  
###  0   Duration  169 non-null    int64  
###  1   Pulse     169 non-null    int64  
###  2   Maxpulse  169 non-null    int64  
###  3   Calories  169 non-null    float64
### dtypes: float64(1), int64(3)
### memory usage: 5.4 KB

df = pd.read_csv('data.csv')
df["Calories"].fillna(130, inplace = True)
print(df.info())
### <class 'pandas.core.frame.DataFrame'>
### RangeIndex: 169 entries, 0 to 168
### Data columns (total 4 columns):
###  #   Column    Non-Null Count  Dtype  
### ---  ------    --------------  -----  
###  0   Duration  169 non-null    int64  
###  1   Pulse     169 non-null    int64  
###  2   Maxpulse  169 non-null    int64  
###  3   Calories  169 non-null    float64
### dtypes: float64(1), int64(3)
### memory usage: 5.4 KB

df = pd.read_csv('data.csv')
x = df["Calories"].mean()
df["Calories"].fillna(x, inplace = True)
print(x)
### 375.79024390243904

df = pd.read_csv('data.csv')
x = df["Calories"].median()
df["Calories"].fillna(x, inplace = True)
print(x)
### 318.6

df = pd.read_csv('data.csv')
x = df["Calories"].mode()[0]
df["Calories"].fillna(x, inplace = True) 
print(x)
### 300.0

df = pd.read_csv('dirtydata.csv')
print(df.info())
### <class 'pandas.core.frame.DataFrame'>
### RangeIndex: 32 entries, 0 to 31
### Data columns (total 5 columns):
### #   Column    Non-Null Count  Dtype  
###---  ------    --------------  -----  
### 0   Duration  32 non-null     int64  
### 1   Date      31 non-null     object 
### 2   Pulse     32 non-null     int64  
### 3   Maxpulse  32 non-null     int64  
### 4   Calories  30 non-null     float64
### dtypes: float64(1), int64(3), object(1)
### memory usage: 1.4+ KB

df['Date'] = pd.to_datetime(df['Date'])
print(df.info())
### <class 'pandas.core.frame.DataFrame'>
### RangeIndex: 32 entries, 0 to 31
### Data columns (total 5 columns):
###  #   Column    Non-Null Count  Dtype         
### ---  ------    --------------  -----         
###  0   Duration  32 non-null     int64         
###  1   Date      31 non-null     datetime64[ns]
###  2   Pulse     32 non-null     int64         
###  3   Maxpulse  32 non-null     int64         
###  4   Calories  30 non-null     float64       
### dtypes: datetime64[ns](1), float64(1), int64(3)
### memory usage: 1.4 KB

df.dropna(subset=['Date'], inplace = True)
print(df.info())

### <class 'pandas.core.frame.DataFrame'>
### Int64Index: 31 entries, 0 to 31
### Data columns (total 5 columns):
###  #   Column    Non-Null Count  Dtype         
### ---  ------    --------------  -----         
###  0   Duration  31 non-null     int64         
###  1   Date      31 non-null     datetime64[ns]
###  2   Pulse     31 non-null     int64         
###  3   Maxpulse  31 non-null     int64         
###  4   Calories  29 non-null     float64       
### dtypes: datetime64[ns](1), float64(1), int64(3)
### memory usage: 1.5 KB


for x in df.index:
    if df.loc[x, "Duration"] > 120:
        df.drop(x, inplace = True)
        print(True)

print(df.duplicated())
### 12     True
df.drop_duplicates(inplace = True) 

print(df.corr())
###           Duration     Pulse  Maxpulse  Calories
### Duration  1.000000 -0.084807 -0.289055  0.323492
### Pulse    -0.084807  1.000000  0.261635  0.508609
### Maxpulse -0.289055  0.261635  1.000000  0.353862
### Calories  0.323492  0.508609  0.353862  1.000000