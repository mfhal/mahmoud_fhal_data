import pandas as pd


def dix_digit(x):
    res = False
    l = len(x)
    if (l == 10) and (x.isdigit()):
        res = True
    return res


def aws(x):
    res = False
    if x in ("Aws", "aws", "AWS"):
        res = True
    return res


df = pd.read_csv('Chain_replacement.csv')
df.info()
for x in df.index:
    if dix_digit(str(df.loc[x, "REPLACING-RENAULT-REF"])) is False:
        df.drop(x, inplace=True)
df.info()


for x in df.index:
    if aws(str(df.loc[x, "REPLACING-SUPPLIER-NAME"])) is False:
        df.drop(x, inplace=True)
df.info()
df["SHIPPING_DATE"] = "2024"
for x in df.index:
    if df.loc[x, "REPLACED-SUPPLIER-REF"] == "CONSOMMABLES":
        df.loc[x, "SHIPPING_DATE"] = "2022"
    elif df.loc[x, "REPLACED-SUPPLIER-REF"] == "NOUVEAU":
        df.loc[x, "SHIPPING_DATE"] = "2023"

df.info()

df['REPLACEMENT-DATE'] = pd.to_datetime(df['REPLACEMENT-DATE'], errors='coerce')
df.sort_values(by='REPLACEMENT-DATE', inplace=True)
df.dropna(subset=['REPLACEMENT-DATE'], inplace=True)
df.drop_duplicates(subset=['REPLACING-RENAULT-REF'], inplace=True)
df.sort_index(inplace=True)

df.fillna("EMPTY", inplace=True)

df.info()

df.to_csv(r'C:\Users\mfhal\Desktop\GitLab\projet data\mahmoud_fhal_data\Module_2\Act-2.6\chain_replacement_clean.csv')







        
