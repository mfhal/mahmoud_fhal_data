CREATE TABLE SPECTACLE (
nospectacle integer PRIMARY KEY,
nom varchar(30) NOT NULL,
duree integer,
type char(10),
CHECK (duree>90 and duree<180),
CHECK (type in ('théâtre', 'danse', 'concert'))
);
drop table spectacle;
drop table salle;
drop table representation;

CREATE TABLE SALLE (
nosalle integer PRIMARY KEY,
nbplaces integer NOT NULL
);

CREATE TABLE REPRESENTATION (
date date PRIMARY KEY,
nospectacle integer,
nosalle integer,
prix integer,
FOREIGN KEY (nospectacle) REFERENCES SPECTACLE(nospectacle),
FOREIGN KEY (nosalle) REFERENCES SALLE(nosalle),
CHECK (prix>15)
);

INSERT INTO SPECTACLE (nospectacle, nom, duree, type)
VALUES (22, 'the joker', 100, 'concert');

INSERT INTO SALLE (nosalle, nbplaces)
VALUES (22, 150);

INSERT INTO REPRESENTATION (date, nospectacle,nosalle, prix)
VALUES ('2022-06-02',22, 22,55);
INSERT INTO REPRESENTATION (date, nospectacle,nosalle, prix)
VALUES ('2015-06-02',22, 22,55);

Select *
from spectacle;

Select *
from salle;

Select *
from representation;