
# Creation table

CREATE TABLE medicament (
nom  VARCHAR (100) PRIMARY KEY,
dsc_fr  VARCHAR (100) ,
dsc_lat  VARCHAR (100) ,
COND  INTEGER ) ;
desc medicament;

CREATE TABLE CI (
ID  VARCHAR (100) PRIMARY KEY,
dsc  VARCHAR (100) ,
medt  VARCHAR (100) ,
 FOREIGN KEY (medt) REFERENCES medicament(nom) ) ;
desc CI;
DROP TABLE medicament;

DROP TABLE CI;
# Alimenter la table

INSERT INTO medicament (nom,dsc_fr,dsc_lat,COND)
VALUES ('chourix','contre la chute des choux','Vivamus','13') ;

INSERT INTO medicament (nom, dsc_fr, dsc_lat, COND)
VALUES ('Tropas', 'contre les dysfonctionnements intellectuels', 'Suspendisse', '42') ;

INSERT INTO CI (ID, dsc,medt)
VALUES ('CI1', 'Ne jamais prendre après minuit', 'Chourix') ;

INSERT INTO CI (ID, dsc,medt)
VALUES ('CI2', 'Ne jamais mettre en contact avec de l-eau', 'Chourix') ;

INSERT INTO CI (ID, dsc,medt)
VALUES ('CI3', 'Garder à l-abri de la lumière du soleil', 'Tropas') ;





# Interroger la table
SELECT *
FROM medicament;
SELECT *
FROM CI;


WHERE pk_numSecu='2820475001124';