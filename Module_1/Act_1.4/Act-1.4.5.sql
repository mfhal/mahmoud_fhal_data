CREATE TABLE  Producteur (
raison_sociale VARCHAR (25) PRIMARY KEY,
ville VARCHAR(255)
);

CREATE TABLE Consommateur (
login VARCHAR(10) ,
email VARCHAR(50) ,
nom VARCHAR(50) NOT NULL,
prenom VARCHAR(50) NOT NULL,
ville VARCHAR(255) NOT NULL,
PRIMARY KEY (login,email),
UNIQUE (nom,prenom,ville)
);

CREATE TABLE Produit (
id INTEGER PRIMARY KEY ,
descr VARCHAR(100),
producteur VARCHAR(25) NOT NULL,
consom_login VARCHAR(10),
consom_email VARCHAR(50),
FOREIGN KEY (producteur) REFERENCES Producteur(raison_sociale),
FOREIGN KEY (consom_login,consom_email) REFERENCES Consommateur(login,email)
);

INSERT INTO Producteur (raison_sociale, ville)
VALUES ('Pommes Picardes SAR', 'Compiègne');

INSERT INTO Consommateur (login, email, nom, prenom, ville)
VALUES ('Al','Al.Un@compiegne.fr','Un','Al','Compiègne');
INSERT INTO Consommateur (login, email, nom, prenom, ville)
VALUES ('Bob','Bob.Deux@compiegne.fr','Deux','Bob','Compiègne');
INSERT INTO Consommateur (login, email, nom, prenom, ville)
VALUES ('Charlie','Charlie.Trois@compiegne.fr','Trois','Charlie','Compiègne');

INSERT INTO Produit (id, descr, producteur)
VALUES (1,'Lot_pommes','Pommes Picardes SAR');

INSERT INTO Produit (id, descr, producteur)
VALUES (2,'Lot_pommes','Pommes Picardes SAR');

INSERT INTO Produit (id, descr, producteur)
VALUES (3,'Lot_pommes','Pommes Picardes SAR');

INSERT INTO Produit (id, descr, producteur)
VALUES (4,'Lot_pommes','Pommes Picardes SAR');

INSERT INTO Produit (id, descr, producteur)
VALUES (5,'Lot_cidre','Pommes Picardes SAR');

INSERT INTO Produit (id, descr, producteur)
VALUES (6,'Lot_cidre','Pommes Picardes SAR');

DROP TABLE Produit;

UPDATE produit
SET consom_login='Al', consom_email='Al.Un@compiegne.fr'
WHERE id=1;

UPDATE produit
SET consom_login='Bob', consom_email='Bob.Deux@compiegne.fr'
WHERE id=2 OR id=3;

UPDATE produit
SET consom_login='Al', consom_email='Al.Un@compiegne.fr'
WHERE id=5 OR id=6;


SELECT *
FROM Produit;


DELETE FROM Consommateur
WHERE login ='Charlie';

SELECT *
FROM Consommateur;
