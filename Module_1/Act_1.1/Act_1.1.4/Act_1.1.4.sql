use lecon1 ;
# Projection, restriction et jointures

#Projection
SELECT nom, prenom 
FROM tEtu;

#Restriction
SELECT * 
FROM tEtu
WHERE nom='Dupont';

#Produit
SELECT *
FROM tEtu,tUv;

#jointure
SELECT *
FROM tEtu JOIN tUv ON pk_numSecu=fk_etu;

SELECT *
FROM tEtu,tUv
WHERE pk_numSecu=fk_etu;


