use lecon1 ;
#Notion de références
CREATE TABLE tUv (
pk_code CHAR(4) NOT NULL,
fk_etu CHAR(13) NOT NULL,
PRIMARY KEY (pk_code, fk_etu),
FOREIGN KEY (fk_etu) REFERENCES tEtu(pk_numSecu));

INSERT INTO tUV (pk_code, fk_etu)
VALUES ('NF17', '1800675001066');
INSERT INTO tUV (pk_code, fk_etu)
VALUES ('NF26', '1800675001066');
INSERT INTO tUV (pk_code, fk_etu)
VALUES ('NF29', '1800675001066');

INSERT INTO tUV (pk_code, fk_etu)
VALUES ('NF17', '2810592012232');
INSERT INTO tUV (pk_code, fk_etu)
VALUES ('NF17', '1700792001278');