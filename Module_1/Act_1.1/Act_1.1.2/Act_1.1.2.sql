use lecon1 ;
#Contrainte de domaine:
INSERT INTO tEtu (pk_numSecu, k_numEtu, nom, prenom)
VALUES ('XXXXXXXXXXXXXXX', 'XXXXXX', 'Dupont', 'Pierre') ;

#Contraintes de clé
INSERT INTO tEtu (pk_numSecu, k_numEtu, nom, prenom)
VALUES ('1800675001066', 'HGYT67655Y', 'Dupont', 'Pierre');
INSERT INTO tEtu (pk_numSecu, k_numEtu, nom, prenom)
VALUES ('2810592012232', 'XGB67668', 'Durand', 'Anne');
INSERT INTO tEtu (pk_numSecu, k_numEtu, nom, prenom)
VALUES ('2810592012232', 'HGYT67655Y', 'Duchemin', 'Aline');

SELECT *
FROM tEtu;

