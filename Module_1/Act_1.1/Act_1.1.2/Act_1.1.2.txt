Contrainte Domaine:

QST1:
ERREUR: valeur trop longue pour le type character(13)
Le nombre des caracteres pour pk_numSecu dépasse celui défini dans la creation de tEtu.

QST2:
INSERT INTO tEtu (pk_numSecu, k_numEtu, nom, prenom)
VALUES ('XXXXXXXXXXXXX', 'XXXX', 'Dupont', 'Pierre') ;

Le nom et prénom sont optionnel ce qui représente une contrainte.
CREATE TABLE tEtu (
pk_numSecu  CHAR(13) PRIMARY KEY ,
k_numEtu  VARCHAR (20) UNIQUE NOT NULL,
nom  VARCHAR (50) NOT NULL,
prenom  VARCHAR (50) ) NOT NULL;



Contrainte Clé:

QST 3:

INSERT INTO tEtu (pk_numSecu, k_numEtu, nom, prenom)
VALUES ('1800675001066', 'AB3937098X', 'Dupont', 'Pierre') ;
INSERT INTO tEtu (pk_numSecu, k_numEtu, nom, prenom)
VALUES ('2820475001124', 'XGB67668', 'Durand', 'Anne') ;
INSERT INTO tEtu (pk_numSecu, k_numEtu, nom, prenom)
VALUES ('2820475001124', 'HGYT67655Y','Duchemin', 'Aline');

ERREUR: la valeur d'une clé dupliquée rompt la contrainte unique « tetu_pkey » DETAIL: La clé « (pk_numsecu)=(1800675001066) » existe déjà

le numSecu représente la clé du tableau elle doit etre unique.

QST 4:

SELECT *
FROM tEtu;

la commande SELECT * nous permet de sélectionner tout le tableau.

QST 5:

Oui on peut insérer dans la table une seconde personne qui aurait comme prénom 'aline' et comme nom 'Duchemin" puisque le caractère unique s'appilque uniquement aux deux premières colonnes.

