use lecon1 ;
# Creation table
CREATE TABLE tetu (
pk_numSecu  CHAR(13) PRIMARY KEY,
k_numEtu  VARCHAR (20) UNIQUE NOT NULL,
nom  VARCHAR (50) ,
prenom  VARCHAR (50) ) ;
desc tetu;
# Alimenter la table
INSERT INTO tEtu (pk_numSecu, k_numEtu, nom, prenom)
VALUES ('1800675001066', 'AB3937098X', 'Dupont', 'Pierre') ;
INSERT INTO tEtu (pk_numSecu, k_numEtu, nom, prenom)
VALUES ('2820475001124', 'XGB67668', 'Durand', 'Anne') ;
# Interroger la table
SELECT pk_numSecu, k_numEtu, nom, prenom
FROM tEtu;
SELECT nom, prenom
FROM tEtu
WHERE pk_numSecu='2820475001124';


