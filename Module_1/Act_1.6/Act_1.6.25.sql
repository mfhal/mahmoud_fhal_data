use db_mahmoud;

### Affichez dans une colonne nommée ’ft5’ le MD5 du numéro de telephone du distributeur ayant l’id 84. 
### Avant le cryptage du numéro il faut lui ajouter le nombre 42 en fin de chaîne puis remplacer les chiffres 7 par des 9.

SELECT md5(concat(replace(telephone,'7','9'),'42')) as 'ft5'
FROM distrib
where id_distrib=84;