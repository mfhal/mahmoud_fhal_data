use db_mahmoud;

SELECT  fiche_personne.nom  , Month(membre.date_inscription) AS 'mois d’abonnement', year (membre.date_inscription) AS 'année d’abonnement'
FROM membre
INNER JOIN fiche_personne ON id_fiche_perso = id_perso;