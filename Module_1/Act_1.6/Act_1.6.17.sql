use db_mahmoud;

### Affichez le nombre total de film visionnés du 30/10/2006 au 27/07/2007 dans une colonne ’films’ 
### en comptant également le nombre de film vus les soirs de Noël (24 décembre de chaque année).
SELECT *
from historique_membre;

SELECT count(id_film) 
from historique_membre
where historique_membre.date BETWEEN '2006-10-30' AND '2007-02-27';

SELECT count(id_film) as nbr_film_noel ,year(historique_membre.date)as year 
from historique_membre
where month(historique_membre.date)=12 and day(historique_membre.date)=24
group by year(historique_membre.date);
