use db_mahmoud;
### Affichez dans 3 colonnes distinctes le nom des membres, leur mois d’abonnement de date d’abonnement et leur année d’abonnement.
### Affichez-les même s’ils n’ont pas de noms.

SELECT  fiche_personne.nom  , Month(membre.date_inscription) AS 'mois d’abonnement', year (membre.date_inscription) AS 'année d’abonnement'
FROM membre
LEFT JOIN fiche_personne ON id_fiche_perso = id_perso;

