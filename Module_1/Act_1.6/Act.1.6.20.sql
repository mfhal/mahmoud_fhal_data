use db_mahmoud;

### Affichez dans une colonne ’uptime’ le nombre de jours absolu séparant le plus ancien visionnage d’un film avec le visionnage le plus récent.
SELECT *
FROM film;

Select  titre, datediff(date_fin_affiche,date_debut_affiche) as uptime
FROM film;