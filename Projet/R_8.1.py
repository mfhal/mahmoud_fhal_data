import pandas as pd
Recruitement_Source_pd = pd.read_excel('C:/Users/mfhal/Desktop/7- Projet final/fichierSource/Recruitment Source.xlsx')

Recruitement_Source_pd["RecruitmentSource"] = Recruitement_Source_pd["RecruitmentSource"].replace(
    ["Diversity Job Fair", "Website Banner Ads", "Internet Search", "On-line Web application"],
    "Website Ads")

Recruitement_Source_pd["RecruitmentSource"] = Recruitement_Source_pd["RecruitmentSource"].replace(
    ["Pay Per Click - Google", "Billboard", "Information Session", "Pay Per Click"],
    "On-campus Recruiting")

Recruitement_Source_pd["RecruitmentSource"] = Recruitement_Source_pd["RecruitmentSource"].replace(
    ["Social Networks - Facebook Twitter etc", "Monster.com", "Newspager/Magazine", "Search Engine - Google Bing Yahoo",
     "MBTA ads"],
    "Social Networks Ads")

Recruitement_Source_pd["RecruitmentSource"] = Recruitement_Source_pd["RecruitmentSource"].replace(
    ["Professional Society", "Glassdoor"],
    "Employee referral")

Recruitement_Source_pd["RecruitmentSource"] = Recruitement_Source_pd["RecruitmentSource"].replace(
    ["Vendor Referral", "Company Intranet - Partner"],
    "Partner referral")

Recruitement_Source_pd["RecruitmentSource"] = Recruitement_Source_pd["RecruitmentSource"].replace(
    ["Indeed", "Careerbuilder"],
    "Other")

Recruitement_Source_pd1 = Recruitement_Source_pd.drop_duplicates(subset=['RecruitmentSource'], keep='first').\
    reset_index(drop=True)

Recruitement_Source_pd1.to_csv('Recruitement_Source.csv', sep=";", index=False)
